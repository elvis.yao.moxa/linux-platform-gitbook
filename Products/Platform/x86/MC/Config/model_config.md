# Model config
This page demonstrates the I/O configuration file of models.

## MC-3201-TL
### mc3201-uart-gpio-config
[include, lang:"sh"](mc3201-uart-gpio-config)

### mc3201-dio-config
[include, lang:"sh"](mc3201-dio-config)