# x86 Platforms

- MC series
  * [MC-3201-TL](/Products/Platform/x86/MC/MC-3201-TL.md)

- Linux Distributions Support
  * [Debian 11](/Products/Platform/x86/LinuxDist/Debian_11.md)
  * [Ubuntu 20.04](/Products/Platform/x86/LinuxDist/Ubuntu_20_04.md)
  * [CentOS 7.9](/Products/Platform/x86/LinuxDist/CentOS_7_9.md)

- Example Code
  * [Example Code](Products/Platform/x86/ExampleCode/README.md)
