# Example Code

- config
  - mc-3201-tl
    - [dio-config](/Products/Platform/x86/ExampleCode/example-code/config/mc-3201-tl/dio-config)
    - [uart-config](/Products/Platform/x86/ExampleCode/example-code/config/mc-3201-tl/uart-config)
- [model](/Products/Platform/x86/ExampleCode/example-code/model)
- [mx-gpio-ctl](/Products/Platform/x86/ExampleCode/example-code/mx-gpio-ctl)
- [mx-dio-ctl](/Products/Platform/x86/ExampleCode/example-code/mx-dio-ctl)
- [mx-gpiochip](/Products/Platform/x86/ExampleCode/example-code/mx-gpiochip)
- [mx-uart-mode](/Products/Platform/x86/ExampleCode/example-code/mx-uart-mode)

## Prerequisite
Choose the model. Edit **model**

For example: MC-3201-TL
```
#!/bin/bash
# Support model name
# mc-3201-tl

MX_MODEL=mc-3201-tl
```

## mx-uart-mode
This example script can help you switch UART mode without controlling GPIO directly.

- Switch port 1 to **RS232** mode.
```sh
./mx-uart-mode set 1 1
Set UART port 1 to RS232 mode
```
- Get port 1 mode.
```sh
./mx-uart-mode get 1
RS232
```

## mx-dio-ctl
This example script can help you change DIO status without controlling GPIO directly.

- Get DI 0 state
```sh
./mx-dio-ctl get di0
0
```
- Get DO 0 state
```sh
./mx-dio-ctl get do0
0
```
- Set DO 0 state as low
```sh
./mx-dio-ctl set do0 0
```
