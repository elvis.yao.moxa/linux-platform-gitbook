#!/bin/bash
#
# mx-gpio-ctl
#
# SPDX-License-Identifier: Apache-2.0
#
# Authors:
#	2021	Elvis Yao <ElvisCW.Yao@moxa.com>
#
# Description:
#	For controlling gpio-sysfs value from Super IO GPIO pin
#

source ./mx-gpiochip

gpio_request() {
	local gpio=${1}

	if [ ! -e "/sys/class/gpio/gpio${gpio}" ]
	then
		echo "${gpio}" > /sys/class/gpio/export
	fi
}

gpio_set_value() {
	local gpio=${1}
	local value=${2}

	gpio_request "${gpio}"
	case "${value}" in
	0)
		echo "low" > "/sys/class/gpio/gpio${gpio}/direction"
		;;
	1)
		echo "high" > "/sys/class/gpio/gpio${gpio}/direction"
		;;
	*)
		usage
	;;
	esac
}

gpio_get_value() {
	local gpio=${1}

	gpio_request "${gpio}"
	cat "/sys/class/gpio/gpio${gpio}/value"
}

check_gpiochip_valid() {
	local chip_label_name=$1

	for gc in /sys/class/gpio/gpiochip*
	do
		if cat $gc/label | grep -q $chip_label_name
		then
			return
		fi
	done

	echo "Invalid gpiochip label name."
	exit 1
}

usage() {
	echo "Usage: $0 [gpiochip label name] [set|get] [index] [value]" >&2
    exit 3
}

if [ $# -lt 3 ]; then
    usage
fi

CHIP_LABEL=$1
check_gpiochip_valid $CHIP_LABEL
gp=${GPIO_CHIP[$CHIP_LABEL]}

case "$2" in
	set)
		gpio_set_value "$($gp $3)" $4
	;;
	get)
		gpio_get_value "$($gp $3)"
	;;
	*)
		usage
	;;
esac
