# Ubuntu 20.04 HWE

- Kernel version: `5.11.0-41-generic`
- ISO: `ubuntu-20.04.3-live-server-amd64.iso`
- Download Link: https://releases.ubuntu.com/20.04/

#### Prerequisite

- Setup network
- Please upgrade kernel image to HWE: https://wiki.ubuntu.com/Kernel/LTSEnablementStack

```
apt update
apt install --install-recommends linux-generic-hwe-20.04

# after reboot, check kernel version and NIC status
uname -a
# Linux moxa 5.11.0-41-generic #45~20.04.1-Ubuntu SMP Wed Nov 10 10:20:10 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
```

- Install packages

```
apt-get install build-essential linux-headers-`uname -r` -y
```

#### To avoid device or resource busy when load gpio-it87 driver

- Add `acpi_enforce_resources=lax` on boot parameter

```
# Edit grub config
vi /etc/default/grub
GRUB_CMDLINE_LINUX="[...] acpi_enforce_resources=lax"

# If you change this file, run 'update-grub' afterwards to update
update-grub

# reboot to let driver load cmdline parameter
```

#### Enforce to use it87-wdt driver instead of Intel iTCO watchdog driver

```
vi /lib/modprobe.d/iTCO-blacklist.conf

blacklist iTCO_wdt
blacklist iTCO_vendor_support
```

#### Build necessary drivers
Please refer to product page: [Product Pages Link](/Products/Platform/x86/README.md)

#### Telit LE910C4 LTE cellular module setting
[Telit/LE910C4](/RF_Modules/LTE/Telit/LE910C4.md)

#### SparkLAN WPEQ-261ACNI(BT) Wi-Fi setting
[Sparklan/WPEQ-261ACNI](/RF_Modules/WiFi/SparkLAN/WPEQ-261ACNI_BT.md)

