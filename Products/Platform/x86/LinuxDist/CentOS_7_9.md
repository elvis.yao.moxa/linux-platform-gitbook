# CentOS 7.9

> **[warning]**  CentOS 7.9 doesn't support Intel Tiger Lake platform

- Kernel version: `3.10.0-1160.21.1.el7.x86_64`
- Download Link: http://isoredirect.centos.org/centos/7/isos/x86_64/

#### Prerequisite

- Update system packages to latest version

```
yum distro-sync
```

- Install packages

```
# install kernel devel and header packages
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum install gcc
```

- After installing the kernel packages, you can find all the kernel headers files in /usr/src/kernels directory using following command.

```
ls /lib/modules/$(uname -r)
ls /usr/src/kernels/$(uname -r)
```

#### To solve 'device or resource busy when load gpio-it87 driver'

- Add `acpi_enforce_resources=lax` on boot parameter

```
# Edit grub config
vi /etc/default/grub
GRUB_CMDLINE_LINUX="[...] acpi_enforce_resources=lax"

# If you change this file, run 'grub2-mkconfig' afterwards to update
grub2-mkconfig -o /etc/grub2.cfg
grub2-mkconfig -o /etc/grub2-efi.cfg
```

#### Enforce to use it87-wdt driver instead of Intel iTCO watchdog driver

```
vi /lib/modprobe.d/iTCO-blacklist.conf

blacklist iTCO_wdt
blacklist iTCO_vendor_support
```

#### Remove it87 series driver from CentOS berfore using Moxa's driver

```
mv /lib/modules/`uname -r`/kernel/drivers/gpio/gpio-it87.ko.xz /lib/modules/`uname -r`/kernel/drivers/gpio/gpio-it87.ko.xz.bak
mv /lib/modules/`uname -r`/kernel/drivers/watchdog/it87_wdt.ko.xz /lib/modules/`uname -r`/kernel/drivers/watchdog/it87_wdt.ko.xz.bak
```

#### Build necessary drivers
Please refer to product page: [Product Pages Link](/Products/Platform/x86/README.md)

#### Telit LE910C4 LTE cellular module setting
[Telit/LE910C4](/RF_Modules/LTE/Telit/LE910C4.md)

#### SparkLAN WPEQ-261ACNI(BT) Wi-Fi setting
[Sparklan/WPEQ-261ACNI](/RF_Modules/WiFi/SparkLAN/WPEQ-261ACNI_BT.md)

