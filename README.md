# Moxa Linux Platform Gitbook

Welcome to Gitbook for Moxa Linux Platform

This Gitbook provides Moxa Linux Platform user guide for Linux developers:
- Model's information
    - Driver porting guide
    - Platform I/O settings
- Prerequisites on Linux distributions
- Example code
- RF modules settings
